import { INCREASE_COUNTER } from './actions';

const INITIAL_STATE = {
  candidateCollection: [
    {
      id: 0,
      name: 'Homer'
    },
    {
      id: 1,
      name: 'Bart'
    }
  ],
  colors: ['#6f42c1', '#fd7e14'],
  voteCounts: {
    id_0: 0,
    id_1: 0,
    total: 0
  }
};

export const reducer = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case INCREASE_COUNTER:
      return {
        ...state,
        voteCounts: {
          ...state.voteCounts,
          ...action.payload
        }
      };
    default:
      return state;
  }
};
